#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <chrono>
#include <future>

template<typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

bool RUNNING = false;

/**
 * Reads file paths from the container file.
 *
 * @param files vector of file names, that will be filled with file paths
 * @param container_file_name file name, which contains all file paths
 */
void read_file_paths(std::vector<std::string>& files, const std::string& container_file_name) {
    std::ifstream container_file;
    container_file.open(container_file_name);

    if (container_file.is_open()) {
        std::string line;
        while (getline(container_file, line)) {
            files.push_back(line);
        }
        container_file.close();
    }
}

/**
 * Helper function to alternate isalpha().
 *
 * @param c input parameter
 * @return boolean value indicating that a symbol is not alpha
 */
bool is_not_alpha(int c) {
    return !isalpha(c);
}

/**
 * Removes punctuation and digits from each string in the words vector.
 *
 * @param words vector of words to be filtered
 */
void remove_punctuation_and_digits(std::vector<std::string>& words) {
    for (auto & word : words) {
        std::string result;
        std::remove_copy_if(
            word.begin(), word.end(),
            std::back_inserter(result), //Store output
            std::ptr_fun<int, bool>(is_not_alpha)
        );
        word = result;
    }
}

/**
 * Transforms each word from the vector to the lower case.
 *
 * @param words vector of words to be filtered
 */
void to_lower_case(std::vector<std::string>& words) {
    for (auto & i : words) {
        std::transform(i.begin(), i.end(), i.begin(), [](unsigned char c) { return std::tolower(c); });
    }
}

/**
 * Puts words from the line into the map containing word frequencies.
 * If word is present -> increment the word counter by 1(map entry value)
 * Else -> put new word into the map with word counter = 1
 *
 * @param word_frequencies map containing word frequencies
 * @param words_on_line words on the line
 */
void put_word_frequency(std::map<std::string, int>& word_frequencies, const std::vector<std::string>& words_on_line) {
    for (auto &word : words_on_line) {
        if (word.empty()) continue;
        int is_word_present = word_frequencies.count(word);

        if (is_word_present > 0) {
            word_frequencies[word] = ++(word_frequencies[word]);
        } else {
            word_frequencies.insert(std::pair<std::string, int>(word, 1));
        }
    }
}

/**
 * Reads the file line by line. Each line is filtered and in the end words from the line
 * are put to the map containing word frequencies.
 *
 * @param word_frequencies map for word frequencies storage
 * @param file
 */
void read_file(std::map<std::string, int>& word_frequencies, const std::string& file) {
    std::string line;
    std::ifstream container_file;
    container_file.open(file);

    if (container_file.is_open()) {
        while (getline(container_file, line)) {
            std::istringstream iss(line);
            std::vector<std::string> words_on_line{ std::istream_iterator<std::string>(iss), {} }; //split by space
            remove_punctuation_and_digits(words_on_line);
            to_lower_case(words_on_line);
            put_word_frequency(word_frequencies, words_on_line);
        }
        container_file.close();
    }
}

/**
 * Writes the word frequencies from the map into output.txt file
 *
 * @param word_frequencies map containing word frequencies
 */
void print_word_frequencies(const std::map<std::string, int>& word_frequencies) {
    std::ofstream output("output.txt");
    output << "I have counted word frequencies for you :)" << "\n";

    for (auto &word_frequency : word_frequencies) {
//        std::cout << word_frequency.first << " " << word_frequency.second << "\n";
        output << word_frequency.first << " " << word_frequency.second << "\n";
    }
    output.close();
}

/**
 * Merges the [second_part] map into [first_part] map
 *
 * @param first_part first map
 * @param second_part second map
 */
void merge_to_map(std::map<std::string, int>& first_part, std::map<std::string, int>& second_part) {
    for (const auto& word_frequency : second_part) {
        if (first_part.count(word_frequency.first) > 0) {
            first_part[word_frequency.first] = first_part[word_frequency.first] + word_frequency.second;
        } else {
            first_part.insert(std::pair<std::string, int>(word_frequency.first, word_frequency.second));
        }
    }
}

/**
 * Reads each file and counts the frequencies.
 *
 * @param files file names
 * @return map containing word frequencies (key = word, value = frequency)
 */
std::map<std::string, int> count_word_frequencies(const std::vector<std::string>& files) {
    std::map<std::string, int> word_frequencies;

    for (const auto & file : files) {
        read_file(word_frequencies, file);
    }
    return word_frequencies;
}

/**
 * Executes the program in multi threaded environment (2 additional threads).
 * Divides the files vector into 2 parts and delegates each part to each thread.
 *
 * @param files file names to be read
 */
void execute_multi_threading(std::vector<std::string>& files) {
    int median = files.size() / 2;

    std::vector<std::string> first_part(files.begin(), files.begin() + median);
    std::vector<std::string> second_part(files.begin() + median, files.end());

    auto first_future = std::async(count_word_frequencies, first_part);
    auto second_future = std::async(count_word_frequencies, second_part);

    std::map<std::string, int> map1 = first_future.get();
    std::map<std::string, int> map2 = second_future.get();

    merge_to_map(map1, map2);
    print_word_frequencies(map1);
}

/**
 * Executes program in single(main) thread.
 *
 * @param files file names to be read
 */
void execute_single_thread(std::vector<std::string>& files) {
    std::map<std::string, int> word_frequencies = count_word_frequencies(files);
    print_word_frequencies(word_frequencies);
}

/**
 * Handles the command from input. Check if multi threading is required or not.
 * If is -> execute the program (count words) in multi threaded environment.
 * Else -> execute the program (count words) in single threaded environment.
 *
 * @param multithreading_tag tag indicating whether to use multi threading or no
 * @param files_container the file name containing all file paths
 */
void handle_command(const std::string& multithreading_tag, const std::string& files_container) {
    auto use_multithreading = multithreading_tag == "multi-thread";
    std::vector<std::string> files;
    read_file_paths(files, files_container);
    auto start = std::chrono::high_resolution_clock::now();
    if (use_multithreading) execute_multi_threading(files);
    else execute_single_thread(files);
    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "Time needed: " << to_ms(end - start).count() << " ms to finish.\n";
}

/**
 * Splits the string by space into vector
 *
 * @param command string to split
 * @return vector containing decomposed string
 */
std::vector<std::string> split_command(const std::string& command) {
    std::istringstream iss(command);
    std::vector<std::string> command_decomposed{ std::istream_iterator<std::string>(iss), {} };
    return command_decomposed;
}

/**
 * Check whether string has specified ending
 *
 * @param fullString string to check
 * @param ending ending to be checked
 * @return boolean value indicating whether string ends with specified ending or no
 */
bool hasEnding(std::string const &fullString, std::string const &ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }
}

/**
 * Check whether input arguments are correct
 *
 * @param multithreading_tag tag indicating whether to use multi threading or no
 * @param files_container_name the file name containing all file paths
 * @return boolean value indicating if arguments are ok or no
 */
bool is_command_valid(const std::string& multithreading_tag, const std::string& files_container_name) {
    if (multithreading_tag != "single-thread" && multithreading_tag != "multi-thread") {
        std::cout << "Unrecognized command ["<< multithreading_tag << "]. Please type --help for more info" << std::endl;
        return false;
    } else if (!hasEnding(files_container_name, ".txt")) {
        std::cout << "Wrong file format. Please check the second provided argument or a file." << std::endl;
        return false;
    }
    return true;
}

/**
 * Parses the commandline and decides what task to execute.
 */
void parse_cmd() {
    std::string command;
    getline(std::cin, command);

    if (command == "--help") {
        std::cout << "The program provides the following commands: " <<
                  "\n - single-thread [files_directory_path]" <<
                  "\n - multi-thread [files_directory_path]";
    } else if (split_command(command).size() == 2) {
        auto args = split_command(command);
        if (!is_command_valid(args[0], args[1])) return;

        handle_command(args[0], args[1]);
        std::cout << "Words have been calculated. Please refer to output.txt file to see the word frequencies" << std::endl;
    } else if (command == "exit") {
        RUNNING = false;
    } else {
        std::cout << "Unrecognized command. Please type --help for more info" << std::endl;
    }
}

/**
 * If program arguments are specified -> count words
 * Else -> ask user for input in standard input
 *
 * @param argc number of arguments
 * @param argv arguments
 * @return
 */
int main(int argc, char *argv[]) {
    if (argc == 3) {
        if (is_command_valid(argv[1], argv[2])) {
            handle_command(argv[1], argv[2]);
            std::cout << "Words have been calculated. Please refer to output.txt file to see the word frequencies" << std::endl;
        }
    }

    RUNNING = true;

    while (RUNNING) {
        std::cout << "Please, type the command. To find out more information type '--help'" << std::endl;
        parse_cmd();
    }
}
