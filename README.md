# Word Frequency Counter

The goal of the Word Frequency Counter project was to implement a word counting program that will take 
a bunch of files, read them and count how many times each word have appeared in each file (by total) and 
write this data to the output file. The program is able to run in both single and multi threaded environments and 
on different OS. User can choose to switch between these implementation by input commands.

## Implementation

In both single and multithreaded implementations when the program starts, it checks whether the program arguments 
were provided. The program arguments have the following format [threading_tag] [filename]:
- [threading_tag] - can be "single-thread"/"multi-thread"
- [filename] - file name, which contains file paths to files with text

The program checks [threading_tag] to find out if it has to use multi threading. If arguments are provided, it counts
the words and outputs them to the "output.txt" file. After that, program can still run and wait for next user commands.
If the program arguments are not provided, program waits for user input(has the same options as listed above).
The user can also type "--help" for more information about the commands.
If the command is incorrect, shows the error - "Unrecognized command. Please type "--help" for more info".
The user can switch between single and multi threaded environments during the program runtime using commands.
For ex.:
- single-thread filepaths.txt -> count words in single main thread
- multi-thread filepaths.txt -> count words in multiple threads (2 additional threads)

1. Single threaded environment -> single main thread does all the job. Reads all the file paths from the container 
file (in my case "filepaths.txt") and reads each file sequentially. Counts the words in each file by reading it 
line by line and putting the words to the map, where key -> word, value -> frequency. Finally writes words with 
their frequencies to the output file ("output.txt"): [word] [frequncy]\n ...

2. Multi threaded environment -> additional two threads added. Main thread reads all the file paths from the container 
file (in my case "filepaths.txt") and puts them into vector of strings. After that, the vector is divided into 2 equal size chunks
and these chunks are delegated to 2 threads that count words from different file sets in parallel. Finally main thread writes
words with their frequencies to the output file ("output.txt"): [word] [frequncy]\n ...
	
	
## Single threaded vs Multi Threaded Performance

- Single threaded: 40ms needed to count words
- Multi threaded: 12ms needed to count words


 

